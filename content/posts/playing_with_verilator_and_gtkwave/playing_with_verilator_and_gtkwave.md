---
weight: 1
title: "Playing with Verilator and GTKWave"
date: 2023-06-15T00:00:00+08:00
lastmod: 2023-06-15T00:00:00+08:00
draft: false
mermaid: true
author: "Pascal"

tags: ["verilator","hardware","gtkwave","simulation"]
categories: ["articles"]

lightgallery: true

toc:
  auto: false
---

## Verilator basics

Basic steps for a simulation of an ALU in Verilator is given in [this link](https://itsembedded.com/dhd/verilator_1/).

## GTKWave customization

There are two way to customize the GTKWave rendering :

- TCL scripts:
  - https://gist.github.com/pcotret/cfff0b6fe449c308f6e34450c11baaa9
  - https://ughe.github.io/2018/11/13/gtkwave-automation
- `gtkwaverc` configuration file:
  - Example: https://github.com/gtkwave/gtkwave/blob/master/gtkwave3-gtk3/examples/gtkwaverc

## Command

```bash
gtkwave -S tcl_script.tcl waveform.vcd 
```

GTKWave will look for `.gtkwaverc` in:

1. Locally where GTKWave is launched.
2. In the user home directory.
