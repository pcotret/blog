---
weight: 1
title: "Rocket RISC-V processor - Dot diagrams"
date: 2023-07-07T00:00:00+08:00
lastmod: 2023-07-07T00:00:00+08:00
draft: false
author: "Pascal"

tags: ["rocket","riscv"]
categories: ["articles"]

lightgallery: true

toc:
  auto: false
---

The Rocket Chip is poorly documented. In order to understand the structure of the Scala code, https://github.com/freechipsproject/diagrammer is a tool able to generate Dot diagrams helping to understand what's inside this processor.

> NB: it has been on Debian Bullseye

```bash
# Prerequisites, dot interactive viewer
sudo apt install xdot
# Cloning the diagram generation tool
git clone https://github.com/freechipsproject/diagrammer
cd diagrammer
git checkout v1.3.3
./diagram.sh -i freechips.rocketchip.system.DefaultConfig.fir --module-name "Rocket" --just-top-level
```

Note that the `*.fir` file is available in the Rocket Chip emulator directory.
Arguments:
- `-i`: input FIR file
- `--module-name`: the top level module of the DOT file
- `--just-top-level`: generates only a DOT file of the module. You can remove this parameter to get a recursive analysis.

- [Sample DOT file](../img/Rocket_hierarchy.dot)
- [Sample SVG file](../img/Rocket_hierarchy.dot.svg)