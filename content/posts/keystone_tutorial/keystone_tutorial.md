---
weight: 1
title: "Running Keystone and a simple enclave application in QEmu"
date: 2025-02-26T00:00:00+08:00
lastmod: 2025-02-26T00:00:00+08:00
draft: false
author: "Pascal"

tags: ["keystone","riscv","tee"]
categories: ["articles"]

lightgallery: true

toc:
  auto: false
---

```bash
export REPO_ROOT=$HOME/keystone
git clone --recurse-submodules https://github.com/keystone-enclave/keystone.git $REPO_ROOT
cd $REPO_ROOT
```

For a first build of a Buildroot-based image:

```bash
make -j$(nproc)
```

The following command will simply run the Buildroot image in QEmu:

```bash
make run
```

- Login: `root` 

- Password: `sifive` 

Then, you will  have to start the Keystone driver:

```bash
modprobe keystone-driver
```

## Running existing examples

Once you're logged in and the Keystone driver is enabled, default examples are available at `/usr/share/keystone/example`.

```bash
# pwd
/usr/share/keystone/examples
# ./hello.ke 
Verifying archive integrity... MD5 checksums are OK. All good.
Uncompressing Keystone Enclave Package
hello, world!
# ./hello-native.ke 
Verifying archive integrity... MD5 checksums are OK. All good.
Uncompressing Keystone Enclave Package
Enclave said: "Hello World"
# ./attestor.ke 
Verifying archive integrity... MD5 checksums are OK. All good.
Uncompressing Keystone Enclave Package
Enclave said value: 5000
Enclave said value: 10000
Attestation report SIGNATURE is valid
Enclave and SM hashes match with expected.
Returned data in the report match with the nonce sent.
# ./tests.ke 
Verifying archive integrity... MD5 checksums are OK. All good.
Uncompressing Keystone Enclave Package
testing test-stack
testing test-loop
testing test-malloc
testing test-long-nop
testing test-fibonacci
testing test-fib-bench
testing test-attestation
Attestation report SIGNATURE is valid
testing test-untrusted
Enclave said: hello world!
Enclave said: 2nd hello world!
Enclave said value: 13
Enclave said value: 20
testing test-data-sealing
Enclave said: Sealing key derivation successful!
```

## Modifying an existing example - Modifying, compiling and running it!

In order to compile enclave applications, Keystone needs a set of environment variables defined in the main [Makefile](https://github.com/keystone-enclave/keystone/blob/master/Makefile). In order to compile the final image, it will be easier to use it.

Once the default Buildroot image has been generated, it will only recompile new elements. Therefore, for simple applications, it will be only a matter of seconds.

```bash
# Examples are in keystone-examples-<some-hex-string>
$ cd $REPO_ROOT/build-generic64/buildroot.build/build/keystone-examples-*
$ nano hello/eapp/hello.c

# Compiling the program image
$ cd $REPO_ROOT/build-generic64/buildroot.build/build/keystone-examples-*
$ make hello-package 
[ 35%] Built target eyrie-hello-eyrie
[ 45%] Built target hello-eyrie
Consolidate compiler generated dependencies of target hello
[ 50%] Building C object hello/CMakeFiles/hello.dir/eapp/hello.c.o
[ 55%] Linking C executable hello
[ 55%] Built target hello
Consolidate compiler generated dependencies of target hello-runner
[ 65%] Built target hello-runner
[ 65%] Generating pkg/hello-runner
[ 70%] Generating pkg/.options_log
[ 75%] Generating pkg/eyrie-rt
[ 80%] Generating pkg/loader.bin
[ 85%] Generating pkg/hello
Header is 717 lines long

WARNING: Overwriting existing file: hello.ke
About to compress 1208 KB of data...
Adding files to archive named "hello.ke"...
./.options_log
./eyrie-rt
./hello
./hello-runner
./loader.bin
CRC: 642448497
MD5: f2f8a9c97426f5ad60b180ef71e666d8

Self-extractable archive "hello.ke" successfully created.
[100%] Built target hello-package
$ cp hello/hello.ke ../../../overlay/root 

# Compiling the new Buildroot image
$ cd $REPO_ROOT
$ make -j$(nproc)
$ make run
```

The new program is located in `/root`: `hello.ke` has been copied to `overlay/root`, this directory can be modified in the Buildroot configuration window available with a `make buildroot-configure` from the repository root.

```bash
# modprobe keystone-driver
[   66.559744] keystone_driver: loading out-of-tree module taints kernel.
[   66.567902] keystone_enclave: keystone enclave v1.0.0
# ls
hello.ke
# ./hello.ke 
Verifying archive integrity... MD5 checksums are OK. All good.
Uncompressing Keystone Enclave Package
hello, Keystone tutorial!
```

## Adding a new example

Based on this, the idea will be to add a new program to this framework. A few questions to be answered:
- [ ] How to add a new program to `keystone-examples` without modifying everything manually?
- [ ] Modify Cmake files?
- [ ] Add targets to the examples Makefile?